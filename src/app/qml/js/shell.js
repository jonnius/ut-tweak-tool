/*
  This file is part of ut-tweak-tool
  Copyright (C) 2018 Imran Iqbal

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License 3 as published by
  the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see http://www.gnu.org/licenses/.
*/

function cmdAndroidGadgetService(action, service) {
    return {
        cmd: 'android-gadget-service ' + action + ' ' + service,
        sudo: false,
    };
}

function cmdMountGetRoRwStatus() {
    var cmd = 'sh ./qml/bin/MountGetRoRwStatus.sh';
    return processLaunch(cmd);
}

function cmdMountSetRoRwStatus(status) {
    var cmd = 'sh ./qml/bin/MountSetRoRwStatus.sh ' + status;
    var sudo = true;
    return processLaunch(cmd, sudo);
}

function cmdServiceUnity8(action) {
    return {
        cmd: action + ' unity8',
        sudo: false,
    };
}

function cmdSystemTheme(opt_selectedTheme) {
    // Differentiate between getting and setting
    if (opt_selectedTheme === undefined) {
        var cmd = 'grep ^theme= ~/.config/ubuntu-ui-toolkit/theme.ini | cut -d. -f4'
    } else {
        var cmd = 'sed -i -e \'s:\\(theme=Ubuntu.Components.Themes.\\).*:\\1' +
            opt_selectedTheme + ':\' ~/.config/ubuntu-ui-toolkit/theme.ini'
    }

    return {
        cmd: cmd,
        sudo: false,
    };
}

function cmdTetheringCheckEnabled() {
    return {
        // cmd: 'grep "^mac-address=\w.*$" /etc/NetworkManager/system-connections/tethering',
        // cmd: 'nmcli c show id tethering | grep "^GENERAL.STATE:.*activated"',
        cmd: 'file /etc/NetworkManager/system-connections/tethering',
        sudo: false,
    };
}

function cmdTetheringRNDIS(action) {
    return {
        cmd: '/usr/bin/tethering ' + action,
        sudo: true,
    };
}

function processLaunch(cmd, opt_useSudo, opt_incStdError, opt_logToConsole) {
    // Default values for optional parameters
    if (opt_useSudo === undefined) opt_useSudo = false;
    if (opt_incStdError === undefined) opt_incStdError = false;
    if (opt_logToConsole === undefined) opt_logToConsole = false;

    var wrapper = '/bin/sh -c "';
    if (opt_useSudo) {
        wrapper += 'echo ' + pamLoader.item.password + ' | sudo -S ';
    }
    wrapper += cmd + '"';
    if (opt_logToConsole) {
        console.log(
            'processLaunch: ' + wrapper.replace(pamLoader.item.password, '****')
        );
    }

    return Process.launch(wrapper, opt_incStdError);
}
