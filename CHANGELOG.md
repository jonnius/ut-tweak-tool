# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

---

## [[0.4.18][0.4.18]] (Xenial) - 2019-09-13
### Fixed
- (#33) Audio: only reset mount to `ro` if it was initially `ro`.
- Replace ampersand with plus sign due to UUITK bug (Apps + Scopes).
- German translation from [@Danfro](https://gitlab.com/Danfro).
- Spanish translation from [@advocatux](https://gitlab.com/advocatux).

---

## [[0.4.17][0.4.17]] (Xenial) - 2019-07-22
### Fixed
- French translation from [@Anne17](https://gitlab.com/Anne17).

---

## [[0.4.16][0.4.16]] (Xenial) - 2019-06-26
### Fixed
- (#20) Prevent cropped pages/text faced when using higher screen scaling values [@cibersheep](https://gitlab.com/cibersheep).
- Improve use of colours and general UI tweaks [@cibersheep](https://gitlab.com/cibersheep).
- Catalan translation from [@cibersheep](https://gitlab.com/cibersheep).

---

## [[0.4.15][0.4.15]] (Xenial) - 2019-06-09
### Fixed
- (#30) Fixed scaling slider to extend beyond beyond lowest and highest default values on current devices.

---

## [[0.4.14][0.4.14]] (Xenial) - 2019-05-26
### Added
- French translation from [@Anne17](https://gitlab.com/Anne17).

---

## [[0.4.13][0.4.13]] (Xenial) - 2019-05-22
### Fixed
- Fixed `System > (USB settings) ADB settings`:
  + (#27) `RNDIS` was broken and required the `tethering` command to also be run.
  + Also prevented the commands being run every time the page is opened.

---

## [[0.4.12][0.4.12]] (Xenial) - 2019-04-11
### Added
- Spanish translation from [@advocatux](https://gitlab.com/advocatux).
- German translation from [@Danfro](https://gitlab.com/Danfro).

---

## [[0.4.11][0.4.11]] (Xenial) - 2019-04-11
### Added
- French translation from [@Anne17](https://gitlab.com/Anne17).

---

## [[0.4.10][0.4.10]] (Xenial) - 2019-04-10
### Added
- Catalan translation from [@cibersheep](https://gitlab.com/cibersheep).

---

## [[0.4.9][0.4.9]] (Xenial) - 2019-04-09
### Added
- Dutch translation from [@SanderKlootwijk](https://gitlab.com/SanderKlootwijk).

### Changed
- Ensure the UTTT app is killed when redeploying to a device using `clickable`.

### Fixed
- Fixed issues when using the SuruDark theme -- big thanks to [@Fusekai](https://gitlab.com/Fusekai) for identifying and resolving these.

---

## [[0.4.8][0.4.8]] (Xenial) - 2018-12-19
### Added
- (#9) Experimental Unity 8 system theme selector for the three themes shipped by default: Ambiance, SuruDark & SuruGradient -- from `Behavior > System theme`.
- Complete Polish translation from [@Daniel20000522](https://gitlab.com/Daniel20000522).

---

## [[0.4.7][0.4.7]] (Xenial) - 2018-12-09
### Added
- (#11) Added the third option of being able to make the image writable temporarily, until the next reboot -- from `System > Make image writable`.
- (#13) Now able to manually restart Unity 8 (i.e. restart the home screen) -- from `System > Services`.
- German translation from [@Danfro](https://gitlab.com/Danfro) x2!
- Catalan translation from [@cibersheep](https://gitlab.com/cibersheep).
- Spanish translation from [@advocatux](https://gitlab.com/advocatux).
- Dutch translation from [@SanderKlootwijk](https://gitlab.com/SanderKlootwijk).

### Changed
- Updated links due to the transfer of the repo to the new maintainer.

### Removed
- (#11) Rebooting no longer required when making the image writable.

---

## [[0.4.6][0.4.6]] (Xenial) - 2018-12-07
### Added
- Dutch translation from [@SanderKlootwijk](https://gitlab.com/SanderKlootwijk).
- Spanish translation from [@advocatux](https://gitlab.com/advocatux).

---

## [[0.4.5][0.4.5]] (Xenial) - 2018-12-05
### Added
- (#10) SSH settings: toggle for enabling/disabling SSH.
- Changelog based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

### Changed
- Centralised app version number.

---

## [0.4.4] (Xenial) - 2018-11-17
- Updated the German translation

---

## [0.4.3] (Xenial)
- Updated the scale option
- Fix the sound notification
- Other small bugs fixed
- Updating translations

---

## [0.4.2] (Xenial)
- Few bug fixed
- Updating translations
- Adding support to change the SMS tone

---

## [0.4.1] (Xenial)
- Adding support for Morph Browser

---

## [0.3.91]
- Fixed issue with Unity8 settings visibility

---

## [0.3.90]
- Fix charset when reading .desktop files, thanks to Michael Zanetti
- [OTA-14] Added option to enable/disable the indicator menu, thanks to Brian Douglass
- [OTA-14] Added option to enable/disable the launcher, thanks to Brian Douglass
- Re-sorted entries for Unity 8 settings
- Updated translation template

---

[Unreleased]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.18...master
[0.4.18]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.17...v0.4.18
[0.4.17]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.16...v0.4.17
[0.4.16]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.15...v0.4.16
[0.4.15]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.14...v0.4.15
[0.4.14]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.13...v0.4.14
[0.4.13]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.12...v0.4.13
[0.4.12]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.11...v0.4.12
[0.4.11]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.10...v0.4.11
[0.4.10]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.9...v0.4.10
[0.4.9]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.8...v0.4.9
[0.4.8]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.7...v0.4.8
[0.4.7]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.6...v0.4.7
[0.4.6]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.5...v0.4.6
[0.4.5]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.4...v0.4.5
